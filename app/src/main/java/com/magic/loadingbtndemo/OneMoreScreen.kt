package com.magic.loadingbtndemo

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.HandlerCompat.postDelayed
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_one_more_screen.*
import kotlinx.android.synthetic.main.content_one_more_screen.*
import loadingbutton.animatedDrawables.ProgressType
import loadingbutton.customViews.CircularProgressButton
import loadingbutton.customViews.ProgressButton

class OneMoreScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_more_screen)
        setSupportActionBar(toolbar)

        var btn = findViewById(R.id.imgBtnTest0) as CircularProgressButton

        btn.setOnClickListener {
            btn.startAnimation();
            btn.doneLoadingAnimation(defaultColor(this), defaultDoneImage(this.resources));
        }

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun defaultColor(context: Context) = ContextCompat.getColor(context, android.R.color.black)

    private fun defaultDoneImage(resources: Resources) =
            BitmapFactory.decodeResource(resources, R.drawable.ic_done_white_48dp)
}
