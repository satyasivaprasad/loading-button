package loadingbutton.animatedDrawables

enum class ProgressType {
    DETERMINATE, INDETERMINATE
}
