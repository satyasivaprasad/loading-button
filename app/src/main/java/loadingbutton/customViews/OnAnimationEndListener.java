package loadingbutton.customViews;

@FunctionalInterface
public interface OnAnimationEndListener {
    void onAnimationEnd();
}
